<?php

namespace Drupal\embla\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Embla Carousel routes.
 */
class EmblaController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
